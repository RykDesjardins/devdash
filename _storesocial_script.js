// CRON entry point
var db = new (require('./_datalayer.js').DataLayer)();
var querystore = new (require('./_querystore.js').QueryStore)();
var _config = require('./_config.js');
var http = require('https');

// Instagram
db.connect().query(
    querystore.socials.getIG(),
    function(err, rows) {
        // Narcity Montreal Token
        var instaToken = "1507478620.1677ed0.2b84f74af9844db3bfb71d7e19187dc1";
        var apiEndpoint = "https://api.instagram.com/v1/users/{0}/?access_token="+instaToken;    
        
        var jsonCallback = function(json) {
            // Prepare SQL inserts
            json = json.data;
            count = json.counts;
            
            db.connect().query(
                querystore.socialhistory.addInstagram(
                    json.id,
                    count.media, 
                    count.followed_by,
                    count.follows
                ), function(err, rows) {
                    if (err) {
                        console.log("Error Instagram History for " + err.message);
                    }   
                }
            );
        };
        
        for (var i = 0; i < rows.length; i++) {
            http.get(apiEndpoint.format(rows[i].id), function(res) {
                var jsonData = "";
                res.on('data', function(d) {
                    jsonData += d;
                });
                
                res.on('end', function() {
                    jsonCallback(JSON.parse(jsonData));
                });
                
            }).on('error', function(e) {
                console.log("Got IG API error: " + e.message);
            });   
        }
    }
);


// Twitter
db.connect().query(
    querystore.socials.getTW(),
    function(err, rows) {
        var Twitter = require('twitter');

        var client = new Twitter({
            consumer_key: _config.sTwitter.key,
            consumer_secret: _config.sTwitter.secret,
            access_token_key: _config.sTwitter.token,
            access_token_secret: _config.sTwitter.tokensecret
        });                    

        var accounts = [];
        var accountIndex = 0;
        var finishUp = function() {
            // Iterate through all accounts  
            for (var i = 0; i < accounts.length; i++) {
                ac = accounts[i];
                
                db.connect().query(
                    querystore.socialhistory.addTwitter(
                        ac.id,
                        ac.statuses_count,
                        ac.followers_count,
                        ac.friends_count
                    ), function(err, rows) {
                        if (err) {
                            console.log("Error Twitter History for " + err.message);
                        }
                    }
                );
            }
        };

        var fetchOneTwitterAccount = function(username, id) {
            client.get('users/show', {
                "screen_name" : username,
                "user_id" : id   
            }, function(err, data) {
                accountIndex++;
                accounts.push(data);

                if (accountIndex == rows.length) {
                    finishUp();   
                } else {
                    ac = rows[accountIndex];
                    fetchOneTwitterAccount(ac.name, ac.id);  
                }
            });
        };

        if (rows.length != 0) {
            fetchOneTwitterAccount(rows[0].name, rows[0].id);
        } else {
            finishUp();   
        }
    }
);  