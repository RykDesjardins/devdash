var http = require("http"),
    url = require("url"),
    path = require("path"),
    fs = require("fs"),
    port = process.argv[2] || 1541,
    rootp = "/usr/share/ryk/devdash/",
    actExec = require('./_actions.js'),
    session = require('sesh/lib/core.js').magicSession(), 
    sockev = new (require('./_socketevent.js'))();

http.createServer(function(request, response) {

  var uri = url.parse(request.url).pathname
    , filename = path.join(rootp, uri);

  if (request.method == "GET") {
      fs.exists(filename, function(exists) {
        if(!exists) {
            var queryVars = url.parse(request.url,true).query;
            actExec.parseExec(request, response, uri.substr(1), queryVars);
        } else {
            if (fs.statSync(filename).isDirectory()) filename += '/index.html';

            fs.readFile(filename, "binary", function(err, file) {
              if(err) {        
                actExec.send404(response);
              } else {
                response.writeHead(200, {
		    "Content-Type": "text/html",
		    "Set-Cookie" : ""
		});
                response.write(file, "binary");
                response.end();   
              }
            });
        }
      });
  } else if (request.method == "POST") {
      var postData = "";
      request.on('data', function(dat) {
          postData += dat;
      });
      request.on('end', function() {
          actExec.parseExec(request, response, uri.substr(1), JSON.parse(postData));
      });
  } else if (request.method == "OPTION") {
    resp.writeHead(200, {
	"Access-Control-Allow-Origin": request.headers.origin,
	"Access-Control-Allow-Methods": "POST, GET, OPTIONS",
	"Access-Control-Allow-Headers": "X-PINGOTHER"
    });
    resp.write("200");
    resp.end();
  }
}).listen(parseInt(port, 10));

sockev.startUp();
