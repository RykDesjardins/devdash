module.exports.QueryStore = function() {
    this.users = {
        "getall" : function() {
            return "SELECT userid, username, email, photoURL, role, active FROM Users";   
        },
        "getone" : function(id) {
            return "SELECT userid, username, email, photoURL, role, active FROM Users WHERE userid=" + id;
        },
        "addone" : function(username, password, email, photoURL, role) {
            return "INSERT INTO Users VALUES(NULL, '{0}', '{1}', '{2}', '{3}', '{4}', 1)".format(
                username,
                password,
                email, 
                photoURL || '',
                role || 'guess'
            );
        },
        "deactivate" : function(userid) {
            return "UPDATE Users SET active = 0 WHERE userid = " + userid;   
        },
        "validate" : function(username, password) {
            return "SELECT userid, username, email, photoURL, role, active FROM Users WHERE {2} = '{0}' AND secret = '{1}'".format(
                username,
                password,
                username.indexOf('@') != -1 ? 'email' : 'username'
            );
        }
    };
    
    this.projects = {
        "getall" : function() {
            return "SELECT proj.*, SUM(mdl.progress) as totalprogress, COUNT(mdl.moduleid) AS totalmodules FROM Projects proj LEFT JOIN Modules mdl ON mdl.projectid = proj.projectid WHERE proj.active = 1 GROUP BY proj.projectid;";   
        },
        "getone" : function(id) {
            return this.getall + " WHERE projectid = " + id;   
        },
        "getmine" : function(userid) {
            return "SELECT proj.*, SUM(mdl.progress) as totalprogress, COUNT(mdl.moduleid) AS totalmodules FROM Projects proj INNER JOIN UserProject user ON user.userid = "+userid+" LEFT JOIN Modules mdl ON mdl.projectid = proj.projectid GROUP BY proj.projectid;";  
        },
        "addone" : function(name, desc, icon, prio, stage, status, eta) {
            return "INSERT INTO Projects VALUES(NULL, '{0}', '{1}', '{2}', {3}, '{4}', '{5}', 1, '{6}')".format(             
                name.replace(/'/g, "''"), 
                desc.replace(/'/g, "''"), 
                icon, 
                prio,
                stage,
                status, 
                eta
            );
        },
        "deactivateone" : function(id) {
            return "UPDATE Projects SET active = 0 WHERE projectid = " + id;   
        }
    };
    
    this.socials = {
        "getIG" : function() {
            return "SELECT * FROM SocialNetworks WHERE type = 'ig'";
        },
        "getTW" : function() {
            return "SELECT * FROM SocialNetworks WHERE type = 'tw'";
        },
        "getFB" : function() {
            return "SELECT * FROM SocialNetworks WHERE type = 'fb'"; 
        }
    };
    
    this.modules = {
        "getall" : function(id) {
            return "SELECT * from Modules WHERE projectid = " + id;
        },
        "getone" : function(id) {
            return "SELECT * from Modules WHERE moduleid = " + id;
        },
        "addone" : function(proj, name, desc, status, progress) {
            return "INSERT INTO Modules VALUES(NULL, {0}, '{1}', '{2}', '{3}', {4}, 1)".format(
                proj, 
                name.replace(/'/g, "''"), 
                desc.replace(/'/g, "''"), 
                status,
                progress
            );    
        },
        "deactivateone" : function(id) {
            return "UPDATE Modules SET active = 0 WHERE moduleid = " + id;   
        },
        "setprogress" : function(id, prog, diff, userid) {
            return "UPDATE Modules SET progress = {0} WHERE moduleid = {1}; INSERT INTO ProgressPoints VALUES({2}, {3}, NOW());".format(
                id, prog, diff, userid
            );   
        }
    };
    
    this.comments = {
        "getall" : function(id) {
            return "SELECT * from Comments WHERE moduleid = " + id;
        },
        "getone" : function(id) {
            return "SELECT * from Comments WHERE commentid = " + id;
        },          
        "addone" : function(moduleid, userid, content) {
            return "INSERT INTO Comments VALUES(NULL, {0}, {1}, '{2}', NOW())".format(
                moduleid, 
                userid, 
                content.replace(/'/g, "''")
            );
        }
    };
    
    this.progress = {
        "getall" : function() {
            return "SELECT at, SUM(count) as points FROM ProgressPoints GROUP BY at ORDER BY at ASC LIMIT 30";   
        },
        "getworkload" : function() {
            return "SELECT SUM(progress) / COUNT(*) as workload FROM Modules WHERE Progress <> 100";   
        }
    };
    
    this.socialhistory = {
        "getInstagram" : function(limit) {
            return "SELECT * FROM (SELECT * From SocialHistory WHERE type = 'ig' ORDER BY at DESC " + (limit?"LIMIT " + limit:"") + ") que ORDER BY que.at ASC";
        },
        "getTwitter" : function(limit) {
            return "SELECT * FROM (SELECT * From SocialHistory WHERE type = 'tw' ORDER BY at DESC " + (limit?"LIMIT " + limit:"") + ") que ORDER BY que.at ASC";
        },
        "addInstagram" : function(id, posts, followers, following) {
            return "INSERT INTO SocialHistory VALUES(NOW(), 'ig', '{0}', {1}, {2}, {3})".format(
                id, posts, followers, following    
            );   
        },
        "addTwitter" : function(id, posts, followers, following) {
            return "INSERT INTO SocialHistory VALUES(NOW(), 'tw', '{0}', {1}, {2}, {3})".format(
                id, posts, followers, following    
            );   
        }
    };
    
    this.captainlogs = {
        "addone" : function(title, owner) {
            return "INSERT INTO CaptainLogs VALUES(NULL, '{0}', NOW(), {1])".format(
                title, owner    
            );
        },
        "getall" : function() {
            return "SELECT log.LogID, log.LogName, log.CreatedOn, log.Owner, user.username as OwnerName FROM CaptainLogs log LEFT JOIN Users user ON user.userid = log.Owner";
        },
        "getone" : function(id) {
            return "SELECT log.LogID, log.LogName, log.CreatedOn, log.Owner, user.username as OwnerName FROM CaptainLogs log LEFT JOIN Users user ON user.userid = log.Owner WHERE log.LogID = " + id;            
        }
    };
    
    this.captainentry = {
        "getall" : function(logid) {
            return "SELECT entry.Content, entry.CreatedOn, entry.Owner, user.username AS OwnerName FROM CaptainLogsEntry entry INNER JOIN Users user ON user.userid = entry.Owner WHERE entry.LogID = " + logid;
        },
        "addone" : function(logid, content, ownerid) {
            return "INSERT INTO CaptainLogsEntry VALUES({0}, '{1}', NOW(), {2})".format(
                logid, 
                content, 
                ownerid.replace(/'/g, "''")
            );
        }
    };
    
    this.updates = {
        "table" : function(table, rows, conditions) {
            first = true;
            sets = "";
            
            for (var row in rows) {
                val = rows[row];
                sets += (first?'':', ') + row + "=" + (typeof (val) === "string" ? "'{0}'".format( val.replace(/'/g, "''")) : val);
                first = false;
            }
            
            return "UPDATE " + table + " SET " + sets + " WHERE " + conditions; 
        }
    };
};
