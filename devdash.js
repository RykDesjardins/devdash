String.prototype.capitalize = function() {
    return this.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
};

if (!String.prototype.format) {
  String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) { 
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
      ;
    });
  };
}

var _global = {
    user : {},
    shortMonths : ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep", "Oct","Nov","Dec"],
    workloadScales : [
        {min:"0" , label: "Small workload"}  ,
        {min:"25", label: "Light workload"}  ,
        {min:"50", label: "Normal worload"}  ,
        {min:"75", label: "Heavy workload"}  ,
        {min:"90", label: "Intense workload"}
    ],
    chartbeat : {
        'mtlblog' : "http://api.chartbeat.com/live/quickstats/v4/?apikey=427b79d605ffec5268ccfd8490bc2c7e&host=mtlblog.com&all_platforms=1",
        'narcity' : "http://api.chartbeat.com/live/quickstats/v4/?apikey=427b79d605ffec5268ccfd8490bc2c7e&host=narcity.com&all_platforms=1"
    },
    state : {
        focus : true,
        init : false
    }
};

var _cache = {
    projects : {},
    tickets : {},
    logs : {},
    socialHistory : {}
}

var DateUtil = new (function() {
    this.toInputString = function(str) {
        if (str) {
            if (str.length > 10) {
                str = str.substr(0, 10);
            } 
        } else {
            str = "";   
        }
        
        return str;
    };
})();

var MainInterface = new (function() {
    var mainObj = undefined;
    var templateObj = undefined;
   
    this.bindWindowSwitch = function() {
        window.onblur = function() {
            _global.state.focus = false;
        };    

        window.focus = function() {
            _global.state.focus = true;
        };
        
        window.onfocus = function() {
            _global.state.focus = true;
        };

        document.onblur = window.onblur;
        document.focus = window.focus;
        document.onfocus = window.onfocus;
    };
    
    this.drawBack = function() {
        mainObj.addClass('drawnback');
        
        setTimeout(
            function() {mainObj.addClass('blurry');},
        500);
    };
    
    this.restore = function() {
        mainObj.removeClass('drawnback');  
        mainObj.removeClass('blurry');
    };
    
    this.showTemplates = function(templateName, cb) {
        _v('.template-card').all(function(card) {
            card.hide(); 
        });
        _v('.' + templateName + '-card', true).show();
        
        this.drawBack();
        
        templateObj.show();
        setTimeout(function() {
            templateObj.addClass('shown'); 
        }, 10);
        
        if (cb) {
            setTimeout(cb, 500);
        }
    }
    
    this.hideTemplates = function(cb) {
        this.restore();
        
        templateObj.removeClass('shown');
        setTimeout(function() {
            templateObj.hide();
            
            if (cb) {
                cb();
            }
        }, 500);
    }
    
    this.slideOut = function() {
        mainObj.addClass('slidOut');
        
        setTimeout(function() {
            mainObj.addClass('blurry');
        }, 500);
    };
    
    this.slideIn = function() {
        mainObj.removeClass('slidOut');  
        mainObj.removeClass('blurry');        
    };    
    
    this.init = function() {
        this.bindWindowSwitch();

        mainObj = _v('main', true);
        templateObj = _v('.templates-overlay', true);
    };
})();

var RealTime = function() {
    var liveSlide = _v('#live-slide', true);
    var isActive = false;
    var cbData;
    
    var updateInterface = function() {
        if (isActive) {
               
        }
    }
    
    var bindRealtimeButton = function() {
        _v('#livetraffic-expand-button', true).bind('click', function() {
            switchToRealTimeSlide();
        });
    };
    
    var bindRealtimeUpdate = function() {
        chartbeat.bindUpdate(function(dat) {
            cbData = dat;
            updateInterface();
        }, 'realtime');
    }
    
    var switchToRealTimeSlide = function() {
        MainInterface.slideOut();
        liveSlide.show();
        
        setTimeout(function() {
            liveSlide.addClass('slidIn');
        }, 20);
        
        chartbeat.blockUIUpdates();
        isActive = true;
    };
    
    var switchBack = function() {
        MainInterface.slideIn();
        liveSlide.removeClass('slidIn');
        
        chartbeat.activateUIUpdates();
        isActive = false;
    };
    
    this.init = function() {
        bindRealtimeButton();  
    };
}

var CaptainLogs = function() {
    var socket;
    var onFocus = false;
    var focusedLogID = -1;
    
    var createLog = function(logid, name, createdon, owner, ownername) {
        _cache.logs[logid] = {
            name : name, 
            createdon : createdon, 
            owner : owner, 
            ownername : ownername,
            entries : []
        }
    };
    
    var createLogEntry = function(logid, content, userid, username) {
        var capLog = _cache.logs[logid];

        if (capLog) {
            capLog.entries.push({
                content : content,
                userid : userid, 
                username : username
            }); 
        }
    };
    
    var eventsReact = {
        "logentryadded" : function(data) {
            createLogEntry(data.logid, data.content, data.userid, data.username);
            updateLogIfInFocus(data.logid);
        }
    };
    
    var updateLogIfInFocus = function(logid) {
        if (onFocus && focusedLogID == logid) {
            // Update main interface on new "Thread"
            (function() {
                setTimeout(function() {
                    
                }, 0);
            })();
        }
    };  
    
    this.init = function() {
        /* 
	socket = io.connect('http://beta.devdash.narcity.com:3001');
        
        for (var ev in eventsReact) {
            socket.on(ev, eventsReact[ev]);   
        }
	*/
    };
};

var UserMenu = function() {
    var menuDropped = false;
    var headerDOMObj = _v('header', true);
    
    var toggleDropMenu = function() {
        if (menuDropped) {
            headerDOMObj.removeClass('dropped');
        } else {
            headerDOMObj.addClass('dropped');
        }   
        
        menuDropped = !menuDropped;
    };
    
    this.bindUserboxButton = function() {
        _v('.header-user-box', true).bind('click', function() {
            toggleDropMenu(); 
        });
    };  
    
    this.bindUserBoxActions = function() {
        _v('.header-user-actions-list li').all(function(li) {
            li.bind('click', function(liobj) {
                window[liobj.data('caller')][liobj.data('action')]();
                toggleDropMenu();
            });
        });
    };
    
    this.init = function() {
        this.bindUserboxButton();
        this.bindUserBoxActions();
    }
};  

var FacebookGraph = function() {
    var accounts = [];
    var fbData = [];
    
    var fillFacebookTable = function() {
        if (fbData.length != 0) {
            table = _v('.facebook-accounts-list', true);
            html = '';
            
            for (var i = 0; i < fbData.length; i++) {
                dat = fbData[i];
                
                if (dat.code == 200) {
                    jsonDat = JSON.parse(dat.body);
                    html += '<tr data-id="{0}"><td><img src="{2}" /><span>{1}</span></td><td>{3}</td></tr>'.format(
                        jsonDat.id,
                        jsonDat.name,
                        jsonDat.picture.data.url,
                        jsonDat.likes
                    );
                }
            }
            
            table.find('tbody', true).html(html);
        }
    }
    
    var fetchPagesData = function() {
        _a.get('listFacebook', {}, function(dbResp) {
            accounts = dbResp;
            someBatch = [];

            for (var i = 0; i < accounts.length; i++) {
                someBatch.push({
                    method: 'GET',
                    relative_url: accounts[i].name + '?fields=id,likes,name,picture'
                })   
            }
            
            // Batch FB API request
            FB.api('/', 'POST', {
                batch:someBatch
            }, function(resp) {
                fbData = resp;
                
                fillFacebookTable();
            });
        });        
    }
    
    this.init = function() {
        window.fbAsyncInit = function() {
            FB.init({
              appId      : '1468563456738953',
              xfbml      : true,
              version    : 'v2.5'
            });
            
            FB.getLoginStatus(function(response){
                fetchPagesData();
            });
        };

        (function(d, s, id){
             var js, fjs = d.getElementsByTagName(s)[0];
             if (d.getElementById(id)) {return;}
             js = d.createElement(s); js.id = id;
             js.src = "//connect.facebook.net/en_US/sdk.js";
             fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));        
    }
}   

var Zendesk = function() {
    var refreshInterval = 1000 * 60 * 10; // 10 minutes
    var intervalID = undefined;
    
    var parseType = function(str) {
        if (str) return str;
        else return 'question';
    }
    
    var sendRequest = function(callback) {
        _a.get('/getZendeskTickets', null, callback);
    };
    
    var displayTicketDetails = function(id, liObject) {
        liObject = liObject ? liObject : _v('.zen-ticket-'+id, true).data('id');
        
        MainInterface.showTemplates('ticket');
    }
    
    var loadAllTickets = function() {
        sendRequest(function(tickets) {
            var ticketUL = _v('.zendesk-ticket-list', true);
            html = "";
            
            if (tickets.count != 0) {
                for (var i = 0; i < tickets.count; i++) {
                    ticket = tickets.tickets[i];
                    html += '<li data-id="{3}" class="zen-ticket-{3}"><img src="uploads/cinema40.png" /><span>{0}<span><span class="color-tag ticket-status-{1}">{1}</span><span class="color-tag ticket-type-{2}">{2}</span></li>'.format(
                        ticket.subject,
                        ticket.status,
                        parseType(ticket.type),
                        ticket.id
                    );   

                    _cache.tickets[ticket.id] = ticket;
                }
            } else {
                html += '<li class="empty-message"><center>Woah. Everything\'s fine!</center></li>';
            }
            
            ticketUL.html(html);
            
            ticketUL.find('li').all(function(obj) {
                obj.bind('click', function(caller) {
                    displayTicketDetails(caller.data('id'), caller);
                });
            });
        });
    };
    
    this.stop = function() {
        clearInterval(intervalID);  
    };
    
    this.run = function() {
        intervalID = setInterval(function() {
            loadAllTickets();
        }, refreshInterval);
        loadAllTickets();
    };
    
    this.init = function() {
        this.run();
    };
};

var Instagram = function() {
    var refreshInterval = 1000 * 60 * 15; // 15 minutes
    var intervalID = undefined;
    
    var instaToken = "182187159.1677ed0.edbcca8943f94a5696437f303a3d8a15";
    var apiEndpoint = "https://api.instagram.com/v1/users/{0}/?access_token="+instaToken;
    
    var accounts = {};
    var history = {};
    
    var historyGraphCanvas = undefined;
    var historyGraph = undefined;
    var chart = undefined;
    
    var updateAccountsStats = function() {
        table = _v('.instagram-accounts-list tbody', true);
        index = 0;
        
        var scheduleErrorRetry = function(acid) {
            setTimeout(function() {
                loadFromAcid(acid);
            }, 5000);
        };
        
        var loadFromAcid = function(acid) {
            _a.getJsonp(apiEndpoint.format(accounts[acid].id), function(igresp, err) {
                li = _v('#ig' + acid, true);
                
                if (!err) {
                    igresp = igresp.data;
                    counts = igresp.counts;

                    li.find('img', true).attr('src', igresp.profile_picture);
                    li.find('.igposts', true).html(counts.media);
                    li.find('.igfollowers', true).html(counts.followed_by);
                    li.find('.igfollowing', true).html(counts.follows);
                    
                    accounts[acid].stats = {
                        posts : counts.media,
                        followers : counts.followed_by,
                        following : counts.follows
                    };
                    accounts[acid].avatar = igresp.profile_picture;
                } else {
                    scheduleErrorRetry(acid);
                }
            }, true);               
        }
        
        for (var acid in accounts) {
            (function(acid) {
                setTimeout(function() {
                    loadFromAcid(acid);
                }, index * 200);
                
                index++;
            })(acid);
        }
    };
    
    var fetchHistory = function() {
        history = {};
        _a.get('getSocialHistory', {network:'ig',limit:12*14}, function(dbResp) {
            // TODO : Add History
            /*
                +------------+------+------------+-------+-----------+-----------+
                | at         | type | id         | posts | followers | following |
                +------------+------+------------+-------+-----------+-----------+
                | 2015-11-01 | ig   | 1507470090 |    78 |      1328 |      1553 |
                +------------+------+------------+-------+-----------+-----------+
            */
            
            for (var i = 0; i < dbResp.length; i++) {
                r = dbResp[i];
                a = history[r.id]; 
                
                if (!a) {
                    a = history[r.id] = [];   
                }
                
                a.push(r);
                history[r.id] = a;
            }
        });
    };
    
    var listAccounts = function() {
        _a.get('listInstagrams', {}, function(ac) {
            if (ac && ac.length != 0) {
                for (var i = 0; i < ac.length; i++) {
                    accounts[ac[i].id] = ac[i];
                }
            }
            
            igLis = '';
            for (acid in accounts) {
                ac = accounts[acid];
                
                igLis += '<tr id="ig{4}" data-acid="{4}"><td><img src="uploads/photo248.png" /><span>{0}</span></td><td class="igposts nomobile">{1}</td><td class="igfollowers">{2}</td><td class="igfollowing nomobile">{3}</td></li>'.format(
                    ac.name,
                    "loading...",
                    "loading...",
                    "loading...",
                    ac.id
                );
            }
            
            _v('.instagram-accounts-list tbody', true).html(igLis);
            
            _v('.instagram-accounts-list tbody tr').all(function(tr) {
                (function(tr) {
                    tr.bind('click', function() {
                        acid = tr.data('acid');
                        
                        ac = accounts[acid];
                        _v('.social-history-posts', true).html(ac.stats.posts);
                        _v('.social-history-followers', true).html(ac.stats.followers);
                        _v('.social-history-following', true).html(ac.stats.following);
                        
                        _v('.social-history-avatar', true).attr('src', ac.avatar);
                        _v('.instagram-history-title', true).removeClass('hidden').find('span', true).html(ac.name);
                        _v('.instagram-real-title', true).addClass('hidden');
                        _v('.instagram-accounts-list', true).addClass('hidden');
                        
                        _v('.instagram-account-history', true).removeClass('hidden');
                        
                        oo = _v('.instagram-history-canvas', true).original();
                        oo.height = oo.style.height = 260;
                        oo.width = oo.style.width = _v('.instagram-account-history', true).original().offsetWidth - 40;
                        context = oo.getContext('2d');
                        
                        if (chart) {
                            chart.destroy();
                        }
                        
                        var chartData = {
                            labels : [],
                            datasets : [{
                                label: "Followers",
                                fillColor: "rgba(51,102,136,0.2)",
                                strokeColor: "rgba(51,102,136,1)",
                                pointColor: "rgba(51,102,136,1)",
                                pointStrokeColor: "#fff",
                                pointHighlightFill: "#fff",
                                pointHighlightStroke: "rgba(51,102,136,1)",
                                data: []                
                            }]
                        };
                        
                        h = history[acid]

                        for (var i = 0; i < h.length; i++) {
                            dateObj = new Date(h[i].at);
                            
                            chartData.labels.push(
                                _global.shortMonths[dateObj.getUTCMonth()] 
                                + " " + 
                                dateObj.getDate()
                            );
                            
                            chartData.datasets[0].data.push(h[i].followers);
                        }
                        
                        chart = new Chart(context).Line(chartData, {
                            scaleFontColor: "rgb(97, 120, 138)",
			    bezierCurve : false
                        });                          
                    });
                })(tr);
            });
            
            _v('.instagram-history-title', true).bind('click', function() {
                _v('.instagram-history-title', true).addClass('hidden');
                _v('.instagram-account-history', true).addClass('hidden');
                _v('.instagram-real-title', true).removeClass('hidden');
                _v('.instagram-accounts-list', true).removeClass('hidden');                 
            });
            
            intervalID = setInterval(function() {
                updateAccountsStats();
            }, refreshInterval);
            updateAccountsStats();
        });
    }
    
    this.init = function() {
        historyGraphCanvas = _v('.instagram-history-canvas', true).original();
        
        listAccounts();
        fetchHistory();
    };
};

var Twitter = function() {
    var refreshInterval = 1000 * 60 * 30; // 20 minutes
    var intervalID = undefined;
    
    var accounts = {};
  
    var list = function() {
        _a.get('/listTwitter', {}, function(ac) {
            accounts = ac;
            
            twLis = '';
            for (var i = 0; i < ac.length; i++) {
                single = ac[i];
                
                if (!single.errors) {
                    twLis += '<tr id="tw{0}"><td><img src="{1}" /><span>{2}</span></td><td class="twposts nomobile">{3}</td><td class="twfollowers">{4}</td><td class="twfollowing nomobile">{5}</td></li>'.format(
                        single.id,
                        single.profile_image_url,
                        single.name,
                        single.statuses_count,
                        single.followers_count,
                        single.friends_count
                    );
                }
            }
            
            _v('.twitter-accounts-list tbody', true).html(twLis);
        });        
    };
    
    this.run = function() {
        intervalID = setInterval(function() {
            list();
        }, refreshInterval);
        list();
    };
    
    this.init = function() {
        this.run();
    };
};

var ChartBeat = function() {
    var chart;
    var context;
    var refreshInterval;
    var callbackCounts = 0;
    var lastMinuteUpdate = 0;
    var counterUp;
    
    var updateHooks = {};
    
    var UIactive = false;
    
    var countUpOptions = {
        useEasing : true, 
        useGrouping : true, 
        separator : ',', 
        decimal : '.', 
        prefix : '', 
        suffix : ' people' 
    };    
    
    var chartData = {
        labels : [],
        datasets : [{
            label: "Chartbeat data",
            fillColor: "rgba(51,102,136,0.2)",
            strokeColor: "rgba(51,102,136,1)",
            pointColor: "rgba(51,102,136,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(51,102,136,1)",
            data: []                
        },{
            label: "MTL Blog",
            fillColor: "rgba(0,0,0,0.2)",
            strokeColor: "rgba(0,0,0,1)",
            pointColor: "rgba(0,0,0,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(0,0,0,1)",
            data: []                
        },{
            label: "Narcity",
            fillColor: "rgba(255,38,52,0.2)",
            strokeColor: "rgba(255,38,52,1)",
            pointColor: "rgba(255,0,0,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(255,38,52,1)",
            data: []                
        }]
    };
    
    var params = {
        'refreshRate' : 4000 // 4 seconds
    };
    
    var traffic = {
        mtlblog : 0,
        narcity : 0
    }; 
    
    var allData = {
        mtlblog: {},
        narcity: {}
    }
    
    this.blockUIUpdates = function() {
        UIactive = false;  
    };
    
    this.activateUIUpdates = function() {
        UIactive = true;  
    }; 
    
    var updateUI = function() {
        if (UIactive && _global.state.focus) {
            label = "";
            date = new Date();
            total = traffic.mtlblog + traffic.narcity;

            if (callbackCounts > 29) {
                chart.removeData();
            } 
            if (lastMinuteUpdate != date.getMinutes()) {
                hours = date.getHours();
                lastMinuteUpdate = date.getMinutes();
                minutes = lastMinuteUpdate;
                minutes = minutes < 10 ? "0" + minutes : minutes;

                label = hours + ':' + minutes;
            }

            counterUp.update(total);
            chart.addData([total, traffic.mtlblog, traffic.narcity], label);
            callbackCounts++;
        }
    }
    
    var updateData = function() {
        accountCounts = 2;
        updatedAccounts = 0;

        _a.get(_global.chartbeat.mtlblog, {}, function(data) {
            traffic.mtlblog = data.data.stats.people;
            allData.mtlblog = data.data;

            updatedAccounts++;
            if (updatedAccounts == accountCounts) {
                updateUI(); 
                executeBindings();
            }
        }, false);

        _a.get(_global.chartbeat.narcity, {}, function(data) {
            traffic.narcity = data.data.stats.people;
            allData.narcity = data.data;
            
            updatedAccounts++;
            if (updatedAccounts == accountCounts) {
                updateUI(); 
                executeBindings();
            }                
        }, false);        
    };
    
    this.stop = function() {
        clearInterval(refreshInterval);
    }
    
    var resizeChartbeat = function() {
        oo = _v('#chartbeat-canvas', true).original();
        oo.style.width = '100%';
        oo.style.height = 'auto';
        context = oo.getContext('2d');        
    };
    
    this.run = function() {
        refreshInterval = setInterval(function() {
            updateData();
        }, params.refreshRate);
        
        updateData();
    };
    
    this.executeBindings = function() {
        setTimeout(function() {
            for (var id in updateHooks) {
                if (typeof updateHooks[id] !== "undefined") {
                    updateHooks[id](allData);   
                }
            }
        }, 1);
    }
    
    this.bindUpdate = function(ftc, id) {
        updateHooks[id] = ftc;
    };
    
    this.removeBinding = function(id) {
        updateHooks[id] = undefined;
        delete updateHooks[id];   
    }; 
    
    this.init = function() {
        context = document.getElementById('chartbeat-canvas').getContext('2d');
        counterUp = new CountUp("chartbeat-realtime", 0, 0, 0, 2.5, countUpOptions);
        
        chart = new Chart(context).Line(chartData, {
            barValueSpacing : 0,
            barShowStroke : false,
            bezierCurve : true
        });
        
        callbackCounts = 0;
        
        window.addEventListener('resize', function() {
            resizeChartbeat(); 
        }, false);
        
        this.activateUIUpdates();
        this.run();
    };
};

var FormValidator = function() {
    // Classes : v-notempty, v-number, v-positive, v-email  
    this.validate = function(form) {
        var valid = true;
        
        form.find('.validated').all(function(input) {
            if (input.hasClass('v-notempty')) {
                if (input.value().trim() == "") {
                    valid = false;
                    input.addClass('invalid');
                } else {
                    input.removeClass('invalid');   
                }
            }
        });
            
        return valid;
    };
    
    this.extractValues = function(form) {
        values = {};
        
        form.find('.validated').all(function(input) {
            values[input.data('formname')] = 
                (input.attr('type') == "password") ? 
                    sha256(input.value()) : 
                    (input.hasClass('capitalize')) ? 
                        input.value().capitalize() : 
                        input.value();
        });
        
        return values;
    }
};

var LoginForm = function() {
    var validator = new FormValidator();
    var loginCallbacks = [];
    
    var prepareInterface = function(user) {
        if (user.length != 0) {
            _v('body', true).removeClass('loggedout');
            _global.user = user[0];
            _v('.header-user-name', true).html(_global.user.username);
            
            if (_global.user.photoURL != 'file.jpg') {
                _v('.header-user-photo', true).attr('src', _global.user.photoURL);
            }

            setTimeout(function() {
                _v('.login-overlay', true).hide();
            }, 500);

            for (var i = 0; i < loginCallbacks.length; i++) {
                loginCallbacks[i]();   
            }
            
            if (_global.user.role != 'admin') {
                _a.load('notadmin.css', 'css');
            }
        } else {
            _v('#TXT_login_secret', true).addClass('invalid');
        }           
    }
    
    this.logout = function() {
        _a.post('/logout', {}, function() {
            loginCallbacks = [function() {
                chartbeat.run();   
            }];
            
            _v('body', true).addClass('loggedout');
            _v('.login-overlay', true).show();
            _v('.login-wrapper', true).addClass('shown');
            _global.user = {};
            chartbeat.stop();
        });
    };
    
    var login = function() {
        castLoading();
        var form = _v('.login-container form', true);
        
        if (validator.validate(form)) { 
            _a.post("/login", validator.extractValues(form), function(user) {
                prepareInterface(user);
                dismisLoading();
            });
        } else {
            dismisLoading();
        }
    };
    
    this.validateCookie = function(callback) {
        _a.get('/validateCookie', {}, function(resp) {
            callback(resp);
        });
    }
    
    this.onlogin = function(cb) {
        loginCallbacks.push(cb);   
    }
    
    var castLoading = function() {
        _v('.login-ajax-loader', true).show();   
    }
    
    var dismisLoading = function() {
        _v('.login-ajax-loader', true).hide();  
    };
    
    this.init = function() {
        this.validateCookie(function(resp) {
            if (resp.authenticated) {
                prepareInterface(resp.user);
            } else {
                _v('#login-letmein', true).bind('click', function(sender) {
                     login();
                });

                _v('.login-container form input').all(function(input) {
                    input.bind('keyup', function(tb, e) {
                        if (e.keyCode == 13) {
                            login();   
                        }
                    });
                });
                
                _v('.login-wrapper', true).addClass('shown');
            }
        });
    };  
};

var ProjectList = function() {
    var saveClickEvent = undefined;
    
    var displayModuleDetails = function(projectid, moduleid) {
        var mod = _cache.projects[projectid].modules[moduleid];
        var saveButton = _v('.module-card-save', true)
        
        _v('.module-card-title', true).value(mod.modulename);
        _v('.module-card-desc', true).value(mod.moduledescription);
        _v('.module-card-status', true).value(mod.modulestatus);
        _v('.module-card-progress', true).value(mod.progress);
        
        if (saveClickEvent) {
            saveButton.unbind('click', saveClickEvent);
        }   
        
        saveClickEvent = saveButton.bind('click', function() {
            newMod = formToObject(moduleid);
            newMod.progressdiff = newMod.progress - mod.progress;

            _a.post('/updateSingleModule', newMod, function() {
                expandProjectModules(projectid);
                progressBoard.fetchWorkload();
                progressBoard.fetchProgressPoints();

                MainInterface.hideTemplates();
            });
        }, true);
        
        MainInterface.showTemplates('module');
    };
    
    var formToObject = function(moduleid) {
        return {
            'id' : moduleid,
            'name' : _v('.module-card-title', true).value(),
            'desc' : _v('.module-card-desc', true).value(),
            'status' : _v('.module-card-status', true).value(),
            'progress' : _v('.module-card-progress', true).value()  
        };
    }
    
    var expandProjectModules = function(projid) {
        listItem = _v('#project' + projid, true);
        
        moduleList = listItem.find('.module-list-wrapper', true);
        
        if (moduleList.hasClass('expanded')) {
            moduleList.removeClass('expanded');    
        } else {
            moduleList.addClass('expanded');
            moduleList.find('p', true).html(_cache.projects[projid].projectdescription.replace(/(?:\r\n|\r|\n)/g, "<br />"));

            cachedModules = _cache.projects[projid].modules = {};
            
            _a.get('/listAllProjectModules', {projectid:projid}, function(mods) {
                moduleList = listItem.find('.module-list', true);
                liStr = '';

                for (var i = 0; i < mods.length; i++) {
                    mod = mods[i];
                    liStr += '<li class="{3}" data-id="{4}"><img src="uploads/game92.png" /><span>{0}</span><span class="color-tag">{1}</span><span class="color-tag perc-tag">{2}</span></li>'.format(
                        mod.modulename,
                        mod.modulestatus,
                        mod.progress + ' %',
                        mod.progress == '100' ? 'semi-trans' : '',
                        mod.moduleid
                    );
                    
                    cachedModules[mod.moduleid] = mod;
                }

                moduleList.html(liStr);
                
                moduleList.find('li').all(function(li) {
                    li.bind('click', function(target) {
                        displayModuleDetails(projid, target.data('id'));
                    });
                });
            });
        }
    };
    
    this.popAddModule = function(projectid) {
        var saveButton = _v('.module-card-save', true)
        
        _v('.module-card-title', true).value("New module");
        _v('.module-card-desc', true).value("Short description");
        _v('.module-card-status', true).value('idea');
        _v('.module-card-progress', true).value("0");
        
        if (saveClickEvent) {
            saveButton.unbind('click', saveClickEvent);
        }   
        
        saveClickEvent = saveButton.bind('click', function() {
            newMod = formToObject(-1);
            newMod.projectid = projectid;

            _a.post('/addModule', newMod, function() {
                expandProjectModules(projectid);
                progressBoard.fetchWorkload();
                progressBoard.fetchProgressPoints();

                MainInterface.hideTemplates();
            });
        }, true);
        
        MainInterface.showTemplates('module');  
        return false; 
    };
    
    var projectFormToObject = function(projid) {
        return {
            'id' : projid,
            'name' : _v('.project-card-title', true).value(),
            'desc' : _v('.project-card-desc', true).value(),
            'status' : _v('.project-card-status', true).value(),
            'stage' : _v('.project-card-stage', true).value(),
            'priority' : parseInt(_v('.project-card-priority', true).value()),
            'eta' : _v('.project-card-eta', true).value()  
        };        
    }    
    
    this.popEditProject = function(projid) {
        var saveButton = _v('.project-card-save', true);
        var proj = _cache.projects[projid];
        
        _v('.project-card-title', true).value(proj.projectname);
        _v('.project-card-desc', true).value(proj.projectdescription);
        _v('.project-card-status', true).value(proj.projectstatus);
        _v('.project-card-stage', true).value(proj.stage);
        _v('.project-card-priority', true).value(proj.priority);
        _v('.project-card-eta', true).value(DateUtil.toInputString(proj.eta));
        
        if (saveClickEvent) {
            saveButton.unbind('click', saveClickEvent);
        }   
        
        saveClickEvent = saveButton.bind('click', function() {
            editProj = projectFormToObject(projid);

            _a.post('/editProject', editProj, function() {
                expandProjectModules(projid);
                progressBoard.fetchWorkload();
                progressBoard.fetchProgressPoints();
                projectList.listProjects();

                MainInterface.hideTemplates();
            });
        }, true);
        
        MainInterface.showTemplates('project');  
        return false;          
    };
    
    this.popAddProject = function() {
        var saveButton = _v('.project-card-save', true);
        
        _v('.project-card-title', true).value("Project name");
        _v('.project-card-desc', true).value("Project description");
        _v('.project-card-status', true).value("idea");
        _v('.project-card-stage', true).value("analysis");
        _v('.project-card-priority', true).value("3");
        _v('.project-card-eta', true).value("");
        
        if (saveClickEvent) {
            saveButton.unbind('click', saveClickEvent);
        }   
        
        saveClickEvent = saveButton.bind('click', function() {
            newProj = projectFormToObject(-1);
 
            _a.post('/addProject', newProj, function() {
                progressBoard.fetchWorkload();
                progressBoard.fetchProgressPoints();
                projectList.listProjects();

                MainInterface.hideTemplates();
            });
        }, true);
        
        MainInterface.showTemplates('project');  
        return false;              
    };
    
    this.popCancelProject = function() {
        projListObj = _v('.cancel-project-list', true);
        lis = "";
        
        for (var projid in _cache.projects) {
            proj = _cache.projects[projid];
            lis += '<li data-id="{1}">{0} ({2}, {3})</li>'.format(
                proj.projectname,
                projid,
                proj.projectstatus,
                proj.stage
            );   
        }
        projListObj.empty().html(lis).find('li').all(function(li) {
            li.bind('click', function(tar) {
                pid = tar.data('id'); 
                
                _a.post('/cancelProject', {id:pid}, function() {
                    progressBoard.fetchWorkload();
                    progressBoard.fetchProgressPoints();
                    projectList.listProjects();

                    MainInterface.hideTemplates();                     
                });
            });
        });
        
        MainInterface.showTemplates('cancel-project');
        return false;
    };

    var updateList = function() {
        listObj = _v('.ongoing-project-list', true);
        finishedListObj = _v('.live-project-list', true);
        liStr = '';
        finishedLiStr = '';
        
        projs = _cache.projects;
        for (var pid in projs) {
            proj = projs[pid];
            progressPerc = proj.totalprogress / proj.totalmodules;
            progressPerc = isNaN(progressPerc) ? 0 : progressPerc;
            
            s = '<li class="project-stage-{1} project-status-{2}" data-progress="{3}" data-id="{5}" id="project{5}"><div class="project-abs-container"><img src="uploads/{4}" /><span>{0}</span><span class="color-tag">{1}</span><span class="color-tag">{2}</span></div><div class="project-progress-bar"></div><div class="module-list-wrapper"><p></p><ul class="module-list"></ul><a href="javascript:projectList.popAddModule({5});" class="admin">Add Module</a><a href="javascript:projectList.popEditProject({5});" class="admin">Edit Project</a></div></li>'.format(
                proj.projectname, 
                proj.stage,
                proj.projectstatus,
                progressPerc,
                proj.projecticon == "" ? "four36.png" : proj.projecticon,
                proj.projectid
            );
            
            if (progressPerc == 100 || proj.stage == "live") {
                finishedLiStr += s;   
            } else {
                liStr += s;   
            }
        }
        
        if ("" != listObj) {
            listObj.html(liStr);
        } else {
            listObj.html('<li class="empty-message"><center>Nothing\'s planned.</center></li>');   
        }
        
        if ("" != finishedListObj) {
            finishedListObj.html(finishedLiStr);    
        } else {
            finishedListObj.html('<li class="empty-message"><center>Nothing\'s running live.</center></li>');   
        }
        
        
        _v('.ongoing-project-list li, .live-project-list li').all(function(li) {
            progress = li.data('progress');
            li.find('.project-abs-container', true).bind('click', function(target) {
                expandProjectModules(_v(target, true).parent().data('id'));
            }); 
            
            li.find('.project-progress-bar', true).style(
                'width', 
                progress + '%' 
            );
            
            if (progress == 100) {
                li.addClass('semi-trans');
            }
        });
    };
    
    this.listProjects = function() {
        _a.get('/listAllProjects', {}, function(projs) {
            for (var i = 0; i < projs.length; i++) {
                _cache.projects[projs[i].projectid] = projs[i];
            }
            
            updateList();
        });
    };
    
    this.initUpdateForm = function() {
        _v('.template-card-dismiss').all(function(btn) {
            btn.bind('click', function() {
                MainInterface.hideTemplates();
            });
        });
    };
    
    this.init = function() {
        this.initUpdateForm();
        this.listProjects();
    };
};

var ProgressBoard = function() {
    var context;
    var serverData;
    var chartData;
    var chart;
    var chartOptions = {
        responsive : true  
    };
    
    var workloadDeg;
    var workloadPerc;
    
    var drawBoard = function() {
        chartData = {
            labels : [],
            datasets : [{
                label: "Daily Progress",
                fillColor: "rgba(51,102,136,0.2)",
                strokeColor: "rgba(51,102,136,1)",
                pointColor: "rgba(51,102,136,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(51,102,136,1)",
                data: []
            }]
        };
        
        for (var i = 0; i < serverData.length; i++) {
            dateobj = new Date(serverData[i].at);
            
            chartData.labels.push(
                _global.shortMonths[dateobj.getUTCMonth()] 
                + " " + 
                dateobj.getDate()
            );
            
            chartData.datasets[0].data.push(serverData[i].points);
        }
        
        oo = _v('#daily-progress-points-canvas', true).original();
        oo.height = oo.style.height = 260;
        oo.width = oo.style.width = window.innerWidth - 450;
        context = oo.getContext('2d');
        
        chart = new Chart(context).Line(chartData, {
            scaleFontColor: "rgb(97, 120, 138)"
        });   
    }
    
    var updateWorkload = function() {
        _v('.speedometer-gauge', true).style('transform', 'rotateZ(' + workloadDeg + 'deg)');
        _v('.speedometer-workload-percent', true).html(Math.round(workloadPerc*100) + " %");
        
        workloadVocabText = "N/A";
        for (var i = 0; i < _global.workloadScales.length; i++) {
            if (workloadPerc*100 > _global.workloadScales[i].min) {
                workloadVocabText = _global.workloadScales[i].label;   
            }
        }
        
        _v('.speedometer-workload-text', true).html(workloadVocabText);
    } 
    
    this.fetchWorkload = function() {
         _a.get('/getWorkload', {}, function(data) {
            workloadPerc = (100 - data[0].workload) / 100;
            workloadDeg = 180 * workloadPerc;
            updateWorkload();
        });          
    }
    
    this.fetchProgressPoints = function() {
        _a.get('/getProgressPoints', {}, function(data) {
            serverData = data;
            drawBoard();
        });        
    }
    
    this.init = function() {
        Chart.defaults.global.responsive = false;
    
        this.fetchProgressPoints();
        this.fetchWorkload();
        
        window.addEventListener('resize', function() {
            drawBoard();
            updateWorkload();
        }, false);
    };
};

// Objects initialization
var loginform     = new LoginForm();
var usermenu      = new UserMenu();
var progressBoard = new ProgressBoard();
var projectList   = new ProjectList();
var chartbeat     = new ChartBeat();
var zendesk       = new Zendesk();
var instagram     = new Instagram();
var twitter       = new Twitter();
var captainlogs   = new CaptainLogs();
var gbGraph       = new FacebookGraph();
var realtime      = new RealTime();

// Main Bootstrapper
var Devdash = function() {    
    this.init = function() {
        MainInterface.init();
        
        loginform.init();
        loginform.onlogin(function() {
            usermenu.init();
            progressBoard.init();
            projectList.init();
            chartbeat.init();
            zendesk.init();
            instagram.init();
            twitter.init();
            captainlogs.init();
            gbGraph.init();
            realtime.init();
        });
       
	_global.state.init = true; 
    };      
};

// Get the whole thing up and running upon DOM ready event fired
var devdash = new Devdash();
document.addEventListener("DOMContentLoaded", function(event) {
    devdash.init();
});
