Devdash Database Structure

Users
	userid
	secret 
	password
	email
	photoURL
	role [admin, analyst, visitor]
	active

QuickTask
	quicktaskid
	userid
	quicktaskname 
	quicktaskdesc
	datestart
	dateend                                                                          

Projects
	projectid
	projectname
	projectdescription
	projecticon
	priority [number, 1 = critical]
	stage [analysis, development, alpha,  beta, live]
	projectstatus [idea, ongoing, pending, blocked, finished]
	active
	eta

UserProject
	projectid
	userid
	role [analyst, support, developer, admin]

Modules
	moduleid
	projectid
	modulename
	moduledescription
	modulestatus [idea, ongoing, pending, block, finished]
	progress [0..100, allow floats]
	active 
	
Comments
	commentid
	moduleid
	userid
	content
	at

 40.759211
-73.984638
