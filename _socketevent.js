var io = require('socket.io')();
var db = new (require('./_datalayer.js').DataLayer)();
var querystore = new (require('./_querystore.js').QueryStore)();
var exec = require('child_process').exec;

var endpoints = {
    "ple" : "pushlogentry",
    "log" : "login"
};

var endpointActions = {
    pushlog : function(socket, d) {
        db.connect().query(
            querystore.captainentry.addone(
                d.id,
                d.t,
                d.u
            ), 
            function(err, rows) {
                socket.emit("logentryadded", {
                    "logid" : d.id,
                    "content" : d.t,
                    "userid" : d.u,
                    "username" : d.un
                });
            }
        );
    },
    login : function(socket, d) {
        
    }  
}

module.exports = function() {
    // Private members
    var that = this; 

    // Private API    
    var bindCustomEvents = function() {
        io.on('connection', function(socket) {
            console.log("[Socket] Received and accepted connection."); 
            
            // Upon log send and update
            for (var endpoint in endpoints) {
                socket.on(endpoint, function(d) {
                    endpointActions[endpoints[endpoint]](socket, d);
                });  
            }
        });
    };
    
    var init = function() {
        bindCustomEvents();
    };
    
    init();    

    // Public API
    this.startUp = function() {
        io.listen(3001);
    };
};