
/* Devdash Database Structure */

CREATE TABLE Users (
	userid                 INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	username               VARCHAR(100),  
	secret                 VARCHAR(64),
	email                  VARCHAR(100),
	photoURL               VARCHAR(300),
	role                   VARCHAR(20),
	active                 TINYINT(1)
);

CREATE TABLE QuickTasks (
	quicktaskid            INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	userid                 INT UNSIGNED,
	quicktaskname          VARCHAR(200),
	quicktaskdesc          TEXT,
	datestart              DATE,
	dateend                DATE 
);

CREATE TABLE Projects (
	projectid              INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	projectname            VARCHAR(200),
	projectdescription     TEXT,
	projecticon            VARCHAR(300),
	priority               SMALLINT,
	stage                  VARCHAR(20),
	projectstatus          VARCHAR(20),
	active                 TINYINT(1),
	eta                    DATE
);

CREATE TABLE UserProject (
	projectid              INT UNSIGNED,
	userid                 INT UNSIGNED,
	role                   VARCHAR(20)
);
    
CREATE TABLE Modules (
	moduleid               INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	projectid              INT UNSIGNED,
	modulename             VARCHAR(200),
	moduledescription      TEXT,
	modulestatus           VARCHAR(20),
	progress               FLOAT,
	active                 TINYINT(1)
);
	
CREATE TABLE Comments (
	commentid              INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	moduleid               INT UNSIGNED,
	userid                 INT UNSIGNED,
	content                TEXT,
	moment                 DATETIME
);
