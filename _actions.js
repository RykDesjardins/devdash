var db = new (require('./_datalayer.js').DataLayer)();
var querystore = new (require('./_querystore.js').QueryStore)();
var zendesk = require('node-zendesk');
var _config = require('./_config.js');
var curl = require('curl');
var exec = require('child_process').exec;
var GA = require('googleanalytics');
var util = require('util');

// var snapchat = require('snapchat');

var deliverParcel = function(resp, parcel) {
    resp.writeHead(200, {"Content-Type": "application/json"});
    resp.write(JSON.stringify(parcel));
    resp.end();
};

var deliverString = function(resp, str) {
    resp.writeHead(200, {"Content-Type": "application/json"});
    resp.write(str);
    resp.end();
};

var userIn = function(req) {return req.session.data&&req.session.data.user!='Guest';};
var isAdmin = function(req) {return req.session.data&&req.session.data.role=='admin';};

var send401 = function(resp) {
    resp.writeHead(401, {"Content-Type": "text/plain"});
    resp.write("401 Forbidden\n");
    resp.end();
    return;
}

// Returns JSON
var actions = {
    loadInitBundle : function(req, resp) {
        deliverParcel(resp, {"message":"Hello, World!"});
    }, 
    validateCookie : function(req, resp) {
        if (userIn(req)) {
            db.connect().query(querystore.users.getone(req.session.data.user), function(err, rows) {
                if (rows.length != 0) {
                    deliverParcel(resp, {
                        'authenticated' : true,
                        'user' : rows
                    });
                } else {
                    deliverParcel(resp, {'authenticated':false});
                }
            });
        } else {
            deliverParcel(resp, {'authenticated':false});
        }
    },
    listAllProjects : function(req, resp) {
        if (userIn(req)) {
            db.connect().query(querystore.projects.getall(), function(err, rows) {
                deliverParcel(resp, rows); 
            });
        } else send401(resp);
    },
    listAllMyProjects : function(req, resp) {
        if (userIn(req)) {
            db.connect().query(querystore.projects.getmine(req.session.data.user), function(err, rows) {
                deliverParcel(resp, rows); 
            });
        } else send401(resp);          
    },
    listAllProjectModules : function(req, resp, dat) {
        if (userIn(req)) {
            db.connect().query(querystore.modules.getall(dat.projectid), function(err, rows) {
                deliverParcel(resp, rows); 
            });
        } else send401(resp);         
    }, 
    updateSingleModule : function(req, resp, dat) {
        if (userIn(req) && isAdmin(req)) {
            db.connect().query(querystore.updates.table(
                'Modules',
                {
                    modulename : dat.name,
                    moduledescription : dat.desc,
                    modulestatus : dat.status,
                    progress : dat.progress
                },
                'moduleid = {0}'.format(dat.id)
            ), function(err, rows) {
                db.connect().query(
                    querystore.modules.setprogress(
                        dat.id, dat.progress, dat.progressdiff, req.session.data.user
                    ), function() {
                        deliverParcel(resp, rows); 
                    }
                );
            });
        } else send401(resp);           
    },
    addModule : function(req, resp, dat) {
        if (userIn(req) && isAdmin(req)) {
            db.connect().query(querystore.modules.addone(
                dat.projectid,
                dat.name, 
                dat.desc,
                dat.status,
                dat.progress
            ), function(err, rows) {
                deliverParcel(resp, rows); 
            });
        } else send401(resp); 
    },
    addProject : function(req, resp, dat) {
        if (userIn(req) && isAdmin(req)) {
            db.connect().query(querystore.projects.addone (
                dat.name, 
                dat.desc,
                '',
                dat.priority,
                dat.stage,
                dat.status,
                dat.eta
            ), function(err, rows) {
                deliverParcel(resp, rows); 
            });
        } else send401(resp);           
    },
    editProject : function(req, resp, dat) {
        if (userIn(req) && isAdmin(req)) {
            db.connect().query(querystore.updates.table(
                'Projects', 
                {
                    projectname : dat.name,
                    projectdescription : dat.desc,
                    projectstatus : dat.status,
                    stage : dat.stage,
                    priority : dat.priority,
                    eta : dat.eta
                },
                'projectid = {0}'.format(dat.id)
            ), function(err, rows) {
                deliverParcel(resp, rows); 
            });
        } else send401(resp);
    },
    listAllUsers : function(req, resp) {
        if (userIn(req)) {
            db.connect().query(querystore.users.getall(), function(err, rows) {
                deliverParcel(resp, rows); 
            });
        } else send401(resp);
    },
    createUser : function(req, resp, dat) {
        username = dat.username;
        password = dat.s;
        email = dat.email;
        photoURL = dat.photo;
        role = dat.role;
        
        db.connect().query(querystore.users.addone (
            username, password, email, photoURL, role
        ), function(err, rows) {
            if (!err) {
                req.session.data.username = username;
                req.session.data.user = rows.insertId;
                req.session.data.role  = role;
                req.session.data.id = req.session.data.user;
            }
            deliverParcel(resp, err||rows.insertId);
        });
    },
    login : function(req, resp, dat) {
        username = dat.u;
        password = dat.s;
        
        db.connect().query(querystore.users.validate(username, password), 
            function(err, rows) {
            if (!err && rows.length != 0) {
                req.session.data.username = rows[0].username;
                req.session.data.email = rows[0].email;
                req.session.data.user = rows[0].userid;
                req.session.data.role = rows[0].role;
                req.session.data.id = req.session.data.user;                
            }
            
            deliverParcel(resp, err||rows); 
        });
    },
    logout : function(req, resp, dat) {
        if (userIn(req)) {
            req.session.data.user = 'Guest';
            deliverParcel(resp, {status:'logged out'});
        } else send401(resp);          
    },
    allAboutMe : function(req, resp, dat) {
        if (userIn(req)) {
            db.connect().query(
                querystore.users.getone(req.session.user.id),                    
                function(err, rows) {
                    deliverParcel(resp, rows); 
                }
            );
        } else send401(resp);   
    },
    createProject : function(req, resp, dat) {
        if (userIn(req) && isAdmin(req)) {
            name = dat.name;
            desc = dat.desc.replace(/'/g, "''");
            icon = dat.icon;
            prio = dat.prio;
            status = dat.status;
            eta = dat.eta;
            db.connect().query(
                querystore.projects.addone(name, desc, icon, prio, status, eta),
                function(err, rows) {
                    deliverParcel(resp, rows);
                }
            );
        } else send401(resp); 
    },
    listInstagrams : function(req, resp) {
        if (userIn(req)) {
            db.connect().query(
                querystore.socials.getIG(),
                function(err, rows) {
                    deliverParcel(resp, rows); 
                }
            );
        } else send401(resp);         
    },
    listFacebook : function(req, resp) { 
        if (userIn(req)) {
            db.connect().query(
                querystore.socials.getFB(),
                function(err, rows) {
                    deliverParcel(resp, rows); 
                }
            );
        } else send401(resp);         
    },     
    getSocialHistory : function(req, resp, dat) {
        if (userIn(req)) {
            switch (dat.network) {
                case 'ig':
                    db.connect().query(
                        querystore.socialhistory.getInstagram(dat.limit),
                        function(err, rows) {
                            deliverParcel(resp, rows); 
                        }
                    );
                    break;
                    
                case 'tw':
                    db.connect().query(
                        querystore.socialhistory.getTwitter(dat.limit),
                        function(err, rows) {
                            deliverParcel(resp, rows); 
                        }
                    );
                    break;
                    
                default:
                    deliverParcel(resp, []);
                    break;
            }
        } else send401(resp);              
    },
    listTwitter : function(req, resp) {
        if (userIn(req)) {
            db.connect().query(
                querystore.socials.getTW(),
                function(err, rows) {
                    var Twitter = require('twitter');

                    var client = new Twitter({
                        consumer_key: _config.twitter.key,
                        consumer_secret: _config.twitter.secret,
                        access_token_key: _config.twitter.token,
                        access_token_secret: _config.twitter.tokensecret
                    });                    
                    
                    var accounts = [];
                    var accountIndex = 0;
                    var finishUp = function() {
                        deliverParcel(resp, accounts);     
                    };
                    
                    var fetchOneTwitterAccount = function(username, id) {
                        client.get('users/show', {
                            "screen_name" : username,
                            "user_id" : id   
                        }, function(err, data) {
                            accountIndex++;
                            accounts.push(data);
                            
                            if (accountIndex == rows.length) {
                                finishUp();   
                            } else {
                                ac = rows[accountIndex];
                                fetchOneTwitterAccount(ac.name, ac.id);  
                            }
                        });
                    };
                    
                    if (rows.length != 0) {
                        fetchOneTwitterAccount(rows[0].name, rows[0].id);
                    } else {
                        deliverParcel();   
                    }
                }
            );            
        } else send401(resp);
    },
    cancelProject : function(req, resp, dat) {
        if (userIn(req) && isAdmin(req)) {
            db.connect().query(
                querystore.projects.deactivateone(dat.id),
                function(err, rows) {
                    deliverParcel(resp, rows);
                }
            );
        } else send401(resp);           
    },
    getProgressPoints : function(req, resp, dat) {
        if (userIn(req)) {
            db.connect().query(
                querystore.progress.getall(),
                function(err, rows) {
                    deliverParcel(resp, rows);   
                }
            );
        } else send401(resp);
    },
    getWorkload : function(req, resp, dat) {
        if (userIn(req)) {
            db.connect().query(
                querystore.progress.getworkload(),
                function(err, rows) {
                    deliverParcel(resp, rows);   
                }
            );
        } else send401(resp);
    },
    getCPU : function(req, resp, dat) {
        if (userIn(req)) {
	    var proc = exec('echo print `top -n 1 | tr -s " " | cut -d$" " -f10 | tail -n +8 | head -n -1 | paste -sd+ | bc`/ `nproc` | python');
	    var output = "";

	    proc.stdout.on('data', function(chunk) {
		output += chunk;
	    });

	    proc.stdout.on('end', function() {
		deliverParcel(resp, {"output":output});
	    });
        } else send401(resp);
    },
    getZendeskTickets : function(req, resp, dat) {
        if (userIn(req)) {
            console.log("[Zendesk] Creating object")
            var client = zendesk.createClient({
                username:  req.session.data.email,
                token:     'sfPhKooOeRarig6ayOR2pGTZuvqkOA1JJ2oyDK1Y',
                remoteUri: 'https://narcity.zendesk.com/api/v2'
            });
            
            console.log('[Zendesk] Requesting tickets');
            client.views.tickets(61591457, function(err, req, zenData) {
                console.log('[Zendesk] Received tickets from API');
                
                if (err) {
                    deliverParcel(resp, []);   
                } else {
                    deliverParcel(resp, zenData[0]);
                }
            });
        } else send401(resp);
    },
	getBitBucketRepo : function(req, resp, dat) {
		// u4YEeWzjg8aByJw27m
        if (userIn(req)) {
            
        } else send401(resp);
	}
};

var actLog = function(req, resp, dat) {
    console.log("Landed in actLog");

    if (_config.ajax.domains.indexOf(req.headers.origin) != -1) {    
        // AJAX call
        resp.writeHead(200, {
	    "Content-Type": "text/plain",
	    "Access-Control-Allow-Origin": req.headers.origin
        });
	resp.write(req.headers.referer);
	resp.end();  
    } else { 
	resp.writeHead(401, {
	    "Content-Type": "text/plain",
	    "Access-Control-Allow-Origin": req.headers.origin
        });
	resp.write('Forbidden');
	resp.end();
    }
}

module.exports = {
    parseExec : function(req, resp, actionName, dat) {
        if (actionName == "log") {
	    actLog(req, resp, dat);
        } else {
            if (actions[actionName]) {
                actions[actionName](req, resp, dat);
            } else {
                this.send404(resp);
            }
        }
    },
    
    send404 : function(resp) {
      resp.writeHead(404, {"Content-Type": "text/plain"});
      resp.write("404 Not Found\n");
      resp.end();
      return;           
    }
};  
