var DataLayer = function() {
	var mysql = require("mysql");
	var creds = require("./_config.js");

	var pool = mysql.createPool(creds.database);

	this.connect = function() {
        return this;
	};

    this.buildSQL = function(sql, arr) {
        for (var i = arr.length; i != 0; i--) {
            sql = sql.replace('%' + i, arr[i-1].toString().replace(/'/g, "''"));
        }
        
        return sql;
    }
    
    this.buildQuery = function(sql, arr, callback, keepConn) {
        this.query(this.buildSQL(sql, arr), callback, keepConn);   
    }
    
	// callback (err, rows)
	this.query = function(query, callback, keepConn) {
		var that = this;
		console.log("[DataLayer.query] " + query);

		pool.getConnection(function(err, connection) {
			try {
				connection.query(query, function(err, rows) {
					if (err) {
						console.log("[DataLayer.query] Error " + err.code);
					} else {
						console.log("[DataLayer.query] Query successfully ran. Executing Callback."); 
					}	
	
					callback(err, rows);

					if (!keepConn) {			
						that.close(connection);
					}
				});
			} catch (ex) {
				console.log("[Datalayer.query] Ouch... Exception here : " + JSON.stringify(ex) + "]");
			}
		});
	};

	this.close = function(connection) {
		try {
			connection.release();
		} catch (ex) {}		
	}
};

module.exports.DataLayer = DataLayer;
